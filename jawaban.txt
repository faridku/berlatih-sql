1. Membuat database

create database myshop;

2. Membuat table di dalam database

create table users (
    id int auto_increment primary key,
    name varchar(255),
    email varchar(255),
    password varchar(255)
    );

create table categories (
    id int auto_increment primary key,
    name varchar(255)
    );

create table items(
    id int auto_increment primary key,
    name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id int,
    foreign key (category_id) references categories(id)
    );


3. Memasukkan data pada table

table users
INSERT INTO users VALUES (NULL, 'John Doe', 'john@doe.com', 'john123');
INSERT INTO users VALUES (NULL, 'Jane Doe', 'jane@doe.com', 'jenita123');

table categories
insert into categories (name) values ('gadget'),('cloth'),('men'),('women'),('branded');

table items
insert into items values (null, 'Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1);
insert into items values (null, 'Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2);
insert into items values (null, 'IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);

4. Mengambil data dari database
a. menampilkan data users kecuali password

select id, name, email from users;

b. menampilkan data items dimana harga lebih dari 1jt

select * from items where price>1000000;

b. menampilkan data items name serupa, kata kunci "uniklo"

select * from items where name like '%uniklo%';

c. menampilkan data items join dengan kategori

select items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori from items join categories on items.category_id=categories.id;

5. Mengubah data dari database

update items set price=2500000 where name like '%sumsang%';




